const jwtConstants = {
    JWT_SECRET: 'secretKey',
    JWT_EXPIRES_IN: '4h',
}
 
export default jwtConstants;