import { Column, Entity, PrimaryGeneratedColumn, OneToMany, ManyToOne } from 'typeorm';
import { TripEntity } from '../trip/trip.entity';

@Entity()
export class MatchEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    pin: string;

    @Column()
    meetingPoint: string;

    @OneToMany(() => TripEntity, trip => trip.match)
    trips: TripEntity[];
}
