import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BusinessError, BusinessLogicException } from '../shared/errors/business-errors';
import { Repository } from 'typeorm';
import { MatchEntity } from './match.entity';
import { randomInt } from 'crypto';

@Injectable()
export class MatchService {
    constructor(
        @InjectRepository(MatchEntity)
        private readonly matchRepository: Repository<MatchEntity>
    ){}

    async findAll(): Promise<MatchEntity[]> {
        return await this.matchRepository.find({ relations: ["trips"] });
    }

    async findOne(id: string): Promise<MatchEntity> {
        const match: MatchEntity = await this.matchRepository.findOne({where: {id}, relations: ["trips"] } );
        if (!match)
          throw new BusinessLogicException("The match with the given id was not found", BusinessError.NOT_FOUND);
    
        return match;
    }
    
    async create(match: MatchEntity): Promise<MatchEntity> 
    {
        match = await this.createPIN(match);
        return await this.matchRepository.save(match);
    }

    async update(id: string, match: MatchEntity): Promise<MatchEntity> {
        const persistedMatch: MatchEntity = await this.matchRepository.findOne({where:{id}});
        if (!persistedMatch)
          throw new BusinessLogicException("The match with the given id was not found", BusinessError.NOT_FOUND);
        
        return await this.matchRepository.save({...persistedMatch, ...match});
    }

    /**
     * Creates a random PIN for the match
     * @param match Match for which the PIN is created
     * @returns an updated match with the PIN
     */
    async createPIN(match: MatchEntity): Promise<MatchEntity>
    {
        match.pin = randomInt.toString();
        return match;
    }

    async delete(id: string) {
        const match: MatchEntity = await this.matchRepository.findOne({where:{id}});
        if (!match)
          throw new BusinessLogicException("The match with the given id was not found", BusinessError.NOT_FOUND);
      
        await this.matchRepository.remove(match);
    }
}
