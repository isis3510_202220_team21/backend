import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { MatchEntity } from './match.entity';
import { MatchService } from './match.service';
import { MatchController } from './match.controller';

@Module({
  imports: [TypeOrmModule.forFeature([MatchEntity])],
  providers: [MatchService],
  controllers: [MatchController]
})
export class MatchModule {}
