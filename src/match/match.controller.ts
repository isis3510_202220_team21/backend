import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, UseInterceptors } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { BusinessErrorsInterceptor } from '../shared/interceptors/business-errors.interceptor';
import { MatchDto } from './match.dto';
import { MatchEntity } from './match.entity';
import { MatchService } from './match.service';

@Controller('matches')
@UseInterceptors(BusinessErrorsInterceptor)
export class MatchController {
    constructor(private readonly matchService: MatchService) {}

    @Get()
    async findAll() {
        return await this.matchService.findAll();
    }

    @Get(':matchId')
    async findOne(@Param('matchId') matchId: string) {
        return await this.matchService.findOne(matchId);
    }

    @Post()
    async create(@Body() matchDto: MatchDto) {
        const match: MatchEntity = plainToInstance(MatchEntity, matchDto);
        return await this.matchService.create(match);
    }

    @Put(':matchId')
    async update(@Param('matchId') matchId: string, @Body() matchDto: MatchDto) {
        const match: MatchEntity = plainToInstance(MatchEntity, matchDto);
        return await this.matchService.update(matchId, match);
    }

    @Delete(':matchId')
    @HttpCode(204)
    async delete(@Param('matchId') matchId: string) {
        return await this.matchService.delete(matchId);
    }
}
