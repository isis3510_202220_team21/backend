import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BusinessError, BusinessLogicException } from '../shared/errors/business-errors';
import { Repository } from 'typeorm';
import { BusEntity } from './bus.entity';

@Injectable()
export class BusService {
    constructor(
        @InjectRepository(BusEntity)
        private readonly busRepository: Repository<BusEntity>
    ){}

    async findAll(): Promise<BusEntity[]> {
        return await this.busRepository.find({ relations: ["reports", "trips"] });
    }

    async findOne(id: string): Promise<BusEntity> {
        const bus: BusEntity = await this.busRepository.findOne({where: {id}, relations: ["reports", "trips"] } );
        if (!bus)
          throw new BusinessLogicException("The bus with the given id was not found", BusinessError.NOT_FOUND);
    
        return bus;
    }
    
    async create(bus: BusEntity): Promise<BusEntity> {
        return await this.busRepository.save(bus);
    }

    async update(id: string, bus: BusEntity): Promise<BusEntity> {
        const persistedBus: BusEntity = await this.busRepository.findOne({where:{id}});
        if (!persistedBus)
          throw new BusinessLogicException("The bus with the given id was not found", BusinessError.NOT_FOUND);
        
        return await this.busRepository.save({...persistedBus, ...bus});
    }

    async delete(id: string) {
        const bus: BusEntity = await this.busRepository.findOne({where:{id}});
        if (!bus)
          throw new BusinessLogicException("The bus with the given id was not found", BusinessError.NOT_FOUND);
      
        await this.busRepository.remove(bus);
    }
}
