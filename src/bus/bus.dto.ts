import {IsNotEmpty, IsString, IsNumber} from 'class-validator';

export class BusDto {
    @IsString()
    @IsNotEmpty()
    readonly name: string;

    @IsNumber()
    @IsNotEmpty()
    readonly direction: number;
}
