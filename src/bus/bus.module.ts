import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { BusEntity } from './bus.entity';
import { BusService } from './bus.service';
import { BusController } from './bus.controller';

@Module({
  imports: [TypeOrmModule.forFeature([BusEntity])],
  providers: [BusService],
  controllers: [BusController]
})
export class BusModule {}
