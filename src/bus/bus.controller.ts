import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, UseInterceptors } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { BusinessErrorsInterceptor } from '../shared/interceptors/business-errors.interceptor';
import { BusDto } from './bus.dto';
import { BusEntity } from './bus.entity';
import { BusService } from './bus.service';

@Controller('buses')
@UseInterceptors(BusinessErrorsInterceptor)
export class BusController {
    constructor(private readonly busService: BusService) {}

    @Get()
    async findAll() {
        return await this.busService.findAll();
    }

    @Get(':busId')
    async findOne(@Param('busId') busId: string) {
        return await this.busService.findOne(busId);
    }

    @Post()
    async create(@Body() busDto: BusDto) {
        const bus: BusEntity = plainToInstance(BusEntity, busDto);
        return await this.busService.create(bus);
    }

    @Put(':busId')
    async update(@Param('busId') busId: string, @Body() busDto: BusDto) {
        const bus: BusEntity = plainToInstance(BusEntity, busDto);
        return await this.busService.update(busId, bus);
    }

    @Delete(':busId')
    @HttpCode(204)
    async delete(@Param('busId') busId: string) {
        return await this.busService.delete(busId);
    }
}
