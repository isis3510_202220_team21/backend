import { StationEntity } from '../station/station.entity';
import { Column, Entity, PrimaryGeneratedColumn, JoinTable, ManyToMany } from 'typeorm';

@Entity()
export class BusEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    name: string;

    @Column()
    direction: number;

    @ManyToMany(() => StationEntity, station => station.buses)
    stations: StationEntity[];
}