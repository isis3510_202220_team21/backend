import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { ReportModule } from './report/report.module';
import { StationModule } from './station/station.module';
import { TripModule } from './trip/trip.module';
import { MatchModule } from './match/match.module';
import { BusModule } from './bus/bus.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './user/user.entity';
import { ReportEntity } from './report/report.entity';
import { StationEntity } from './station/station.entity';
import { TripEntity } from './trip/trip.entity';
import { MatchEntity } from './match/match.entity';
import { BusEntity } from './bus/bus.entity';
import { UserReportsModule } from './user-reports/user-reports.module';
import { UserTripsModule } from './user-trips/user-trips.module';
import { TripStationsModule } from './trip-stations/trip-stations.module';
import { MatchTripsModule } from './match-trips/match-trips.module';
import { StationReportsModule } from './station-reports/station-reports.module';
import { StationBusesModule } from './station-buses/station-buses.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [UserModule, ReportModule, StationModule, TripModule, MatchModule, BusModule, 
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      url: process.env.DATABASE_URL,
      type: 'postgres',
      ssl: {
        rejectUnauthorized: false,
      }, 
      host: process.env.POSTGRES_HOST || 'localhost',
      port: 5432,
      username: process.env.POSTGRES_USERNAME || 'postgres',
      password: process.env.POSTGRES_PASSWORD || 'postgres',
      database: process.env.POSTGRES_DATABASE || 'moviles',
      entities: [UserEntity, ReportEntity, StationEntity, TripEntity, MatchEntity, BusEntity],
      dropSchema: false,
      synchronize: true,
      keepConnectionAlive: true
    }),
    UserReportsModule,
    UserTripsModule,
    TripStationsModule,
    MatchTripsModule,
    StationReportsModule,
    StationBusesModule,
    AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}