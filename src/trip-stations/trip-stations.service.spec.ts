import { Test, TestingModule } from '@nestjs/testing';
import { TripStationsService } from './trip-stations.service';

describe('TripStationsService', () => {
  let service: TripStationsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TripStationsService],
    }).compile();

    service = module.get<TripStationsService>(TripStationsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
