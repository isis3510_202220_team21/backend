import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { StationEntity } from '../station/station.entity';
import { TripEntity } from '../trip/trip.entity';
import { Repository } from 'typeorm';
import { BusinessError, BusinessLogicException } from '../shared/errors/business-errors';

@Injectable()
export class TripStationsService {
    constructor(
        @InjectRepository(TripEntity)
        private readonly tripRepository: Repository<TripEntity>,

        @InjectRepository(StationEntity)
        private readonly stationRepository: Repository<StationEntity>
    ) {}

    async addStationTrip(tripId: string, stationId: string): Promise<TripEntity> {
        const station: StationEntity = await this.stationRepository.findOne({where: {id: stationId}});
        if (!station)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND);
       
        const trip: TripEntity = await this.tripRepository.findOne({where: {id: tripId}, relations: ["stations"]}) 
        if (!trip)
          throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND);
     
        trip.stations = [...trip.stations, station];
        return await this.tripRepository.save(trip);
      }
     
    async findStationByTripIdStationId(tripId: string, stationId: string): Promise<StationEntity> {
        const station: StationEntity = await this.stationRepository.findOne({where: {id: stationId}});
        if (!station)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND)
        
        const trip: TripEntity = await this.tripRepository.findOne({where: {id: tripId}, relations: ["stations"]}); 
        if (!trip)
          throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND)
    
        const tripStation: StationEntity = trip.stations.find(e => e.id === station.id);
    
        if (!tripStation)
          throw new BusinessLogicException("The station with the given id is not associated to the trip", BusinessError.PRECONDITION_FAILED)
    
        return tripStation;
    }
     
    async findStationsByTripId(tripId: string): Promise<StationEntity[]> {
        const trip: TripEntity = await this.tripRepository.findOne({where: {id: tripId}, relations: ["stations"]});
        if (!trip)
          throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND)
        
        return trip.stations;
    }
     
    async associateStationsTrip(tripId: string, stations: StationEntity[]): Promise<TripEntity> {
        const trip: TripEntity = await this.tripRepository.findOne({where: {id: tripId}, relations: ["stations"]});
     
        if (!trip)
          throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND)
     
        for (let i = 0; i < stations.length; i++) {
          const station: StationEntity = await this.stationRepository.findOne({where: {id: stations[i].id}});
          if (!station)
            throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND)
        }
     
        trip.stations = stations;
        return await this.tripRepository.save(trip);
      }
     
    async deleteStationTrip(tripId: string, stationId: string){
        const station: StationEntity = await this.stationRepository.findOne({where: {id: stationId}});
        if (!station)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND)
     
        const trip: TripEntity = await this.tripRepository.findOne({where: {id: tripId}, relations: ["stations"]});
        if (!trip)
          throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND)
     
        const tripStation: StationEntity = trip.stations.find(e => e.id === station.id);
     
        if (!tripStation)
            throw new BusinessLogicException("The station with the given id is not associated to the trip", BusinessError.PRECONDITION_FAILED)

        trip.stations = trip.stations.filter(e => e.id !== stationId);
        await this.tripRepository.save(trip);
    }   
}
