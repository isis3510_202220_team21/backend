import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { TripStationsService } from './trip-stations.service';
import { TripEntity } from '../trip/trip.entity';
import { StationEntity } from '../station/station.entity';
import { TripStationsController } from './trip-stations.controller';

@Module({
  imports: [TypeOrmModule.forFeature([TripEntity, StationEntity])],
  providers: [TripStationsService],
  controllers: [TripStationsController]
})
export class TripStationsModule {}
