import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, UseInterceptors } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { StationDto } from '../station/station.dto';
import { StationEntity } from '../station/station.entity';
import { BusinessErrorsInterceptor } from '../shared/interceptors/business-errors.interceptor';
import { TripStationsService } from './trip-stations.service';

@Controller('trips')
@UseInterceptors(BusinessErrorsInterceptor)
export class TripStationsController {
    constructor(private readonly tripStationService: TripStationsService){}

    @Post(':tripId/stations/:stationId')
    async addStationTrip(@Param('tripId') tripId: string, @Param('stationId') stationId: string){
        return await this.tripStationService.addStationTrip(tripId, stationId);
    }

    @Get(':tripId/stations/:stationId')
    async findStationByTripIdStationId(@Param('tripId') tripId: string, @Param('stationId') stationId: string){
        return await this.tripStationService.findStationByTripIdStationId(tripId, stationId);
    }

    @Get(':tripId/stations')
    async findStationsByTripId(@Param('tripId') tripId: string){
        return await this.tripStationService.findStationsByTripId(tripId);
    }

    @Put(':tripId/stations')
    async associateStationsTrip(@Body() stationsDto: StationDto[], @Param('tripId') tripId: string){
        const stations = plainToInstance(StationEntity, stationsDto)
        return await this.tripStationService.associateStationsTrip(tripId, stations);
    }
    
    @Delete(':tripId/stations/:stationId')
    @HttpCode(204)
    async deleteStationTrip(@Param('tripId') tripId: string, @Param('stationId') stationId: string){
        return await this.tripStationService.deleteStationTrip(tripId, stationId);
    }
}
