import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReportEntity } from '../report/report.entity';
import { StationEntity } from '../station/station.entity';
import { StationReportsService } from './station-reports.service';
import { StationReportsController } from './station-reports.controller';

@Module({
  imports: [TypeOrmModule.forFeature([StationEntity, ReportEntity])],
  providers: [StationReportsService],
  controllers: [StationReportsController]
})
export class StationReportsModule {}
