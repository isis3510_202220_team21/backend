import { Test, TestingModule } from '@nestjs/testing';
import { StationReportsService } from './station-reports.service';

describe('StationReportsService', () => {
  let service: StationReportsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StationReportsService],
    }).compile();

    service = module.get<StationReportsService>(StationReportsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
