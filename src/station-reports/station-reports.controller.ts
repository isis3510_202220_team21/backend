import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, UseInterceptors } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { ReportDto } from '../report/report.dto';
import { ReportEntity } from '../report/report.entity';
import { BusinessErrorsInterceptor } from '../shared/interceptors/business-errors.interceptor';
import { StationReportsService } from './station-reports.service';

@Controller('stations')
@UseInterceptors(BusinessErrorsInterceptor)
export class StationReportsController {
    constructor(private readonly stationReportService: StationReportsService){}

    @Post(':stationId/reports/:reportId')
    async addReportStation(@Param('stationId') stationId: string, @Param('reportId') reportId: string){
        return await this.stationReportService.addReportStation(stationId, reportId);
    }

    @Get(':stationId/reports/:reportId')
    async findReportByStationIdReportId(@Param('stationId') stationId: string, @Param('reportId') reportId: string){
        return await this.stationReportService.findReportByStationIdReportId(stationId, reportId);
    }

    @Get(':stationId/reports')
    async findReportsByStationId(@Param('stationId') stationId: string){
        return await this.stationReportService.findReportsByStationId(stationId);
    }

    @Put(':stationId/reports')
    async associateReportsStation(@Body() reportsDto: ReportDto[], @Param('stationId') stationId: string){
        const reports = plainToInstance(ReportEntity, reportsDto)
        return await this.stationReportService.associateReportsStation(stationId, reports);
    }
    
    @Delete(':stationId/reports/:reportId')
    @HttpCode(204)
    async deleteReportStation(@Param('stationId') stationId: string, @Param('reportId') reportId: string){
        return await this.stationReportService.deleteReportStation(stationId, reportId);
    }
}
