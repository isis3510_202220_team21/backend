import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ReportEntity } from '../report/report.entity';
import { StationEntity } from '../station/station.entity';
import { StationService } from '../station/station.service';
import { ReportService } from '../report/report.service';
import { Repository } from 'typeorm';
import { BusinessError, BusinessLogicException } from '../shared/errors/business-errors';

@Injectable()
export class StationReportsService {
    constructor(
        @InjectRepository(StationEntity)
        private readonly stationRepository: Repository<StationEntity>,

        @InjectRepository(ReportEntity)
        private readonly reportRepository: Repository<ReportEntity>
    ) {}

    async addReportStation(stationId: string, reportId: string): Promise<StationEntity> {
        const report: ReportEntity = await this.reportRepository.findOne({where: {id: reportId}});
        const totalReports = await (await this.reportRepository.find()).length;

        if (!report)
          throw new BusinessLogicException("The report with the given id was not found", BusinessError.NOT_FOUND);
       
        const station: StationEntity = await this.stationRepository.findOne({where: {id: stationId}, relations: ["reports", "buses"]}) 
        if (!station)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND);
     
        station.reports = [...station.reports, report];
        station.riskScore = (station.reports.length / totalReports)*10;
        return await this.stationRepository.save(station);
    }
     
    async findReportByStationIdReportId(stationId: string, reportId: string): Promise<ReportEntity> {
        const report: ReportEntity = await this.reportRepository.findOne({where: {id: reportId}});
        if (!report)
          throw new BusinessLogicException("The report with the given id was not found", BusinessError.NOT_FOUND)
        
        const station: StationEntity = await this.stationRepository.findOne({where: {id: stationId}, relations: ["reports"]}); 
        if (!station)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND)
    
        const stationReport: ReportEntity = station.reports.find(e => e.id === report.id);
    
        if (!stationReport)
          throw new BusinessLogicException("The report with the given id is not associated to the station", BusinessError.PRECONDITION_FAILED)
    
        return stationReport;
    }
     
    async findReportsByStationId(stationId: string): Promise<ReportEntity[]> {
        const station: StationEntity = await this.stationRepository.findOne({where: {id: stationId}, relations: ["reports"]});
        if (!station)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND)
        
        return station.reports;
    }
     
    async associateReportsStation(stationId: string, reports: ReportEntity[]): Promise<StationEntity> {
        const station: StationEntity = await this.stationRepository.findOne({where: {id: stationId}, relations: ["reports"]});
     
        if (!station)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND)
     
        for (let i = 0; i < reports.length; i++) {
          const report: ReportEntity = await this.reportRepository.findOne({where: {id: reports[i].id}});
          if (!report)
            throw new BusinessLogicException("The report with the given id was not found", BusinessError.NOT_FOUND)
        }
     
        station.reports = reports;
        return await this.stationRepository.save(station);
    }
     
    async deleteReportStation(stationId: string, reportId: string){
        const report: ReportEntity = await this.reportRepository.findOne({where: {id: reportId}});
        if (!report)
          throw new BusinessLogicException("The report with the given id was not found", BusinessError.NOT_FOUND)
     
        const station: StationEntity = await this.stationRepository.findOne({where: {id: stationId}, relations: ["reports"]});
        if (!station)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND)
     
        const stationReport: ReportEntity = station.reports.find(e => e.id === report.id);
     
        if (!stationReport)
            throw new BusinessLogicException("The report with the given id is not associated to the station", BusinessError.PRECONDITION_FAILED)

        station.reports = station.reports.filter(e => e.id !== reportId);
        await this.stationRepository.save(station);
    }  
}
