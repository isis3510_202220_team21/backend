import {IsNotEmpty, IsString} from 'class-validator';

export class TripDto {
    @IsString()
    @IsNotEmpty()
    readonly date: string;

    @IsString()
    @IsNotEmpty()
    readonly startTime: string;

    @IsString()
    @IsNotEmpty()
    readonly endTime: string;
}