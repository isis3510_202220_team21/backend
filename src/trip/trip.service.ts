import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BusinessError, BusinessLogicException } from '../shared/errors/business-errors';
import { Repository } from 'typeorm';
import { TripEntity } from './trip.entity';

@Injectable()
export class TripService {
    constructor(
        @InjectRepository(TripEntity)
        private readonly tripRepository: Repository<TripEntity>
    ){}

    async findAll(): Promise<TripEntity[]> {
        return await this.tripRepository.find({ relations: ["stations", "user", "match"] });
    }

    async findOne(id: string): Promise<TripEntity> {
        const trip: TripEntity = await this.tripRepository.findOne({where: {id}, relations: ["stations", "user", "match"] } );
        if (!trip)
          throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND);
    
        return trip;
    }
    
    async create(trip: TripEntity): Promise<TripEntity> {
        trip.state = "Unmatched";
        return await this.tripRepository.save(trip);
    }

    async update(id: string, trip: TripEntity): Promise<TripEntity> {
        const persistedTrip: TripEntity = await this.tripRepository.findOne({where:{id}});
        if (!persistedTrip)
          throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND);
        
        return await this.tripRepository.save({...persistedTrip, ...trip});
    }

    /**
     * Updates the state of the trip with the given id.
     * @param id trip id
     * @param state new state
     * @returns updated trip information
     */
    async updateTripState(trip: TripEntity, state:string):Promise<TripEntity>{     
        trip.state = state;
        return await this.update(trip.id, trip);
    }

    async cancelTrip(id: string) {
        var trip:TripEntity = await this.findOne(id);
        if (!trip)
            throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND);
        
        trip = await this.updateTripState(trip, "Cancelled");
        return trip;
    }

    async delete(id: string) {
        const trip: TripEntity = await this.tripRepository.findOne({where:{id}});
        if (!trip)
          throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND);
      
        await this.tripRepository.remove(trip);
    }
}
