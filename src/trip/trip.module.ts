import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { TripEntity } from './trip.entity';
import { TripService } from './trip.service';
import { TripController } from './trip.controller';

@Module({
  imports: [TypeOrmModule.forFeature([TripEntity])],
  providers: [TripService],
  controllers: [TripController]
})
export class TripModule {}
