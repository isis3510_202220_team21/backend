import { Column, Entity, PrimaryGeneratedColumn, OneToMany, ManyToOne } from 'typeorm';
import { UserEntity } from '../user/user.entity';
import { StationEntity } from '../station/station.entity';
import { MatchEntity } from '../match/match.entity';

@Entity()
export class TripEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    date: string;

    @Column()
    state: string;

    @Column()
    startTime: string;

    @Column()
    endTime: string;

    @ManyToOne(() => UserEntity, user => user.trips)
    user: UserEntity;

    @OneToMany(() => StationEntity, station => station.trip)
    stations: StationEntity[];

    @ManyToOne(() => MatchEntity, match => match.trips)
    match: MatchEntity;
}