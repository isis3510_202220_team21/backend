import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, UseInterceptors } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { BusinessErrorsInterceptor } from '../shared/interceptors/business-errors.interceptor';
import { TripDto } from './trip.dto';
import { TripEntity } from './trip.entity';
import { TripService } from './trip.service';

@Controller('trips')
@UseInterceptors(BusinessErrorsInterceptor)
export class TripController {
    constructor(private readonly tripService: TripService) {}

    @Get()
    async findAll() {
        return await this.tripService.findAll();
    }

    @Get(':tripId')
    async findOne(@Param('tripId') tripId: string) {
        return await this.tripService.findOne(tripId);
    }

    @Post()
    async create(@Body() tripDto: TripDto) {
        const trip: TripEntity = plainToInstance(TripEntity, tripDto);
        return await this.tripService.create(trip);
    }

    @Put(':tripId')
    async update(@Param('tripId') tripId: string, @Body() tripDto: TripDto) {
        const trip: TripEntity = plainToInstance(TripEntity, tripDto);
        return await this.tripService.update(tripId, trip);
    }

    @Put('/cancel/:tripId')
    async cancel(@Param('tripId') tripId: string) {
        return await this.tripService.cancelTrip(tripId);
    }

    @Delete(':tripId')
    @HttpCode(204)
    async delete(@Param('tripId') tripId: string) {
        return await this.tripService.delete(tripId);
    }
}
