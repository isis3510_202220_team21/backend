import { Column, Entity, PrimaryGeneratedColumn, JoinTable, OneToMany, ManyToOne, ManyToMany } from 'typeorm';
import { TripEntity } from '../trip/trip.entity';
import { ReportEntity } from '../report/report.entity';
import { BusEntity } from '../bus/bus.entity';

@Entity()
export class StationEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    name: string;

    // longitud , latitud
    @Column()
    location: string;

    @Column()
    riskScore: number;

    @Column()
    zone: string;

    @ManyToOne(() => TripEntity, trip => trip.stations)
    trip: TripEntity;

    @OneToMany(() => ReportEntity, report => report.station)
    reports: ReportEntity[];

    @ManyToMany(() => BusEntity, bus => bus.stations)
    @JoinTable()
    buses: BusEntity[];
}
