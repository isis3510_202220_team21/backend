import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { StationEntity } from './station.entity';
import { StationService } from './station.service';
import { StationController } from './station.controller';

@Module({
  imports: [TypeOrmModule.forFeature([StationEntity])],
  providers: [StationService],
  controllers: [StationController]
})
export class StationModule {}
