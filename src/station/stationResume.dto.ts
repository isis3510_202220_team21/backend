import {IsNotEmpty, IsString, IsNumber, IsArray} from 'class-validator';

export class StationResumDto {
    @IsString()
    @IsNotEmpty()
    id: string;

    @IsString()
    @IsNotEmpty()
    readonly name: string;

    @IsString()
    @IsNotEmpty()
    readonly location: string;

    @IsNumber()
    @IsNotEmpty()
    readonly riskScore: number;

    @IsString()
    @IsNotEmpty()
    readonly zone: string;

    @IsArray()
    @IsNotEmpty()
    readonly reportTypes: string[];
}