import {IsNotEmpty, IsString, IsNumber} from 'class-validator';

export class StationDto {
//    @IsString()
//    @IsNotEmpty()
//    id: string;

    @IsString()
    @IsNotEmpty()
    readonly name: string;

    @IsString()
    @IsNotEmpty()
    readonly location: string;

    @IsString()
    @IsNotEmpty()
    readonly zone: string;
}
