import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BusinessError, BusinessLogicException } from '../shared/errors/business-errors';
import { Repository } from 'typeorm';
import { StationEntity } from './station.entity';
import { StationResumDto } from './stationResume.dto';

@Injectable()
export class StationService {
    constructor(
        @InjectRepository(StationEntity)
        private readonly stationRepository: Repository<StationEntity>
    ){}

    async findAll(): Promise<StationEntity[]> {
      return await this.stationRepository.find({ relations: ["reports", "buses"] });
    }

    async findAllResume(): Promise<any> {
      const stationsList: StationEntity[] = await this.stationRepository.find({ relations: ["reports"] });
      var stationsResume: StationResumDto[] = [];
      for (var val of stationsList) {
        const reports = val.reports;
        var reportTypes: string[] = [];
        for (var report of reports) {
          if (!reportTypes.find(element => element == report.type)) {
            reportTypes.push(report.type)
          }
        }
        const station: StationResumDto = {
          id: val.id,
          name: val.name,
          zone: val.zone,
          location: val.location,
          reportTypes: reportTypes,
          riskScore: val.riskScore          
        }
        stationsResume.push(station);
      }
      return stationsResume;
    }

    async findOne(id: string): Promise<StationEntity> {
        const station: StationEntity = await this.stationRepository.findOne({where: {id}, relations: ["reports", "buses"] } );
        if (!station)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND);
    
        return station;
    }

    async findByName(pName: string) {
        const station:StationEntity = await this.stationRepository.findOne({where:{name:pName}});
        if (!station)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND);
    
        return station;
    }
    
    async create(station: StationEntity): Promise<StationEntity> {
        station.riskScore = 0;
        return await this.stationRepository.save(station);
    }

    async update(id: string, station: StationEntity): Promise<StationEntity> {
        const persistedStation: StationEntity = await this.stationRepository.findOne({where:{id}});
        if (!persistedStation)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND);
        
        return await this.stationRepository.save({...persistedStation, ...station});
    }

    async delete(id: string) {
        const station: StationEntity = await this.stationRepository.findOne({where:{id}});
        if (!station)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND);
      
        await this.stationRepository.remove(station);
    }
}
