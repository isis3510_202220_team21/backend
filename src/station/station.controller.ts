import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, UseInterceptors } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { BusinessErrorsInterceptor } from '../shared/interceptors/business-errors.interceptor';
import { StationDto } from './station.dto';
import { StationEntity } from './station.entity';
import { StationService } from './station.service';

@Controller('stations')
@UseInterceptors(BusinessErrorsInterceptor)
export class StationController {
    constructor(private readonly stationService: StationService) {}

    @Get()
    async findAll() {
        return await this.stationService.findAll();
    }

    @Get('resume')
    async findAllResume() {
        return await this.stationService.findAllResume();
    }

    @Get(':stationId')
    async findOne(@Param('stationId') stationId: string) {
        return await this.stationService.findOne(stationId);
    }

    @Get('/estaciones/:stationName')
    async findByName(@Param('stationName') stationName: string) {
        return await this.stationService.findByName(stationName);
    }

    @Post()
    async create(@Body() stationDto: StationDto) {
        const station: StationEntity = plainToInstance(StationEntity, stationDto);
        return await this.stationService.create(station);
    }

    @Put(':stationId')
    async update(@Param('stationId') stationId: string, @Body() stationDto: StationDto) {
        const station: StationEntity = plainToInstance(StationEntity, stationDto);
        return await this.stationService.update(stationId, station);
    }

    @Delete(':stationId')
    @HttpCode(204)
    async delete(@Param('stationId') stationId: string) {
        return await this.stationService.delete(stationId);
    }
}
