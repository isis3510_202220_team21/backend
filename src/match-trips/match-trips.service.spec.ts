import { Test, TestingModule } from '@nestjs/testing';
import { MatchTripsService } from './match-trips.service';

describe('MatchTripsService', () => {
  let service: MatchTripsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MatchTripsService],
    }).compile();

    service = module.get<MatchTripsService>(MatchTripsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
