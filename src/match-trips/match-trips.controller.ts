import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, UseInterceptors } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { TripDto } from '../trip/trip.dto';
import { TripEntity } from '../trip/trip.entity';
import { BusinessErrorsInterceptor } from '../shared/interceptors/business-errors.interceptor';
import { MatchTripsService } from './match-trips.service';

@Controller('matches')
@UseInterceptors(BusinessErrorsInterceptor)
export class MatchTripsController {
    constructor(private readonly matchTripService: MatchTripsService){}

    @Post(':matchId/trips/:tripId')
    async addTripMatch(@Param('matchId') matchId: string, @Param('tripId') tripId: string){
        return await this.matchTripService.addTripMatch(matchId, tripId);
    }

    @Get(':matchId/trips/:tripId')
    async findTripByMatchIdTripId(@Param('matchId') matchId: string, @Param('tripId') tripId: string){
        return await this.matchTripService.findTripByMatchIdTripId(matchId, tripId);
    }

    @Get(':matchId/trips')
    async findTripsByMatchId(@Param('matchId') matchId: string){
        return await this.matchTripService.findTripsByMatchId(matchId);
    }

    @Get('verify/:tripId')
    async verify(@Param('tripId') tripId: string){
        return await this.matchTripService.verify(tripId);
    }

    @Put(':matchId/trips')
    async associateTripsMatch(@Body() tripsDto: TripDto[], @Param('matchId') matchId: string){
        const trips = plainToInstance(TripEntity, tripsDto)
        return await this.matchTripService.associateTripsMatch(matchId, trips);
    }
    
    @Delete(':matchId/trips/:tripId')
    @HttpCode(204)
    async deleteTripMatch(@Param('matchId') matchId: string, @Param('tripId') tripId: string){
        return await this.matchTripService.deleteTripMatch(matchId, tripId);
    }
}
