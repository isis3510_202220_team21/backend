import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MatchEntity } from '../match/match.entity';
import { TripEntity } from '../trip/trip.entity';
import { Repository } from 'typeorm';
import { BusinessError, BusinessLogicException } from '../shared/errors/business-errors';

@Injectable()
export class MatchTripsService {
    constructor(
        @InjectRepository(MatchEntity)
        private readonly matchRepository: Repository<MatchEntity>,

        @InjectRepository(TripEntity)
        private readonly tripRepository: Repository<TripEntity>
    ) {}

    async addTripMatch(matchId: string, tripId: string): Promise<MatchEntity> {
        const trip: TripEntity = await this.tripRepository.findOne({where: {id: tripId}});
        if (!trip)
          throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND);
       
        const match: MatchEntity = await this.matchRepository.findOne({where: {id: matchId}, relations: ["trips", "trips"]}) 
        if (!match)
          throw new BusinessLogicException("The match with the given id was not found", BusinessError.NOT_FOUND);
     
        const validate = await this.validateMatch(trip, match);
        if (validate)
        {
          // cambiar el estado del trip 
          // trip.state = "Matched";
          // TODO Hacer update del trip para persistirlo
          match.trips = [...match.trips, trip];
        }
        else 
        {
          throw new BusinessLogicException("The trip cannot be added to the match", BusinessError.NOT_FOUND);
        }

        return await this.matchRepository.save(match);
    }
    
    /**
     * Validates if a trip can be added to a match
     * @param trip trip to be matched
     * @param match match in which a new trip is going to be added
     * @returns 
     */
    async validateMatch(trip:TripEntity, match:MatchEntity) : Promise<boolean>
    {
      var validation:boolean = false;

      if( match.trips.length == 0)
      {
        validation = true;
      }

      if(match.trips.length == 1)
      {
        var trip1 = match.trips[0];
        if(trip1.stations[0] == trip.stations[0] && trip1.stations[1] == trip.stations[1])
          validation = true;
      }

      return validation;
    }

    /**
     * Verifies if a given trip can be matched with an existing match.
     * @param tripId Id of the trip to verify
     * @returns match to which the trip can be matched
     */
    async verify(tripId:string): Promise<MatchEntity> {
      const trip: TripEntity = await this.tripRepository.findOne({where: {id: tripId}});
        if (!trip)
          throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND)
      
      // Gets all the existent matches in the DB
      const matches:MatchEntity[] = await this.matchRepository.find();

      var val:boolean = false;
      var match:MatchEntity = null;
      for( let m of matches)
      {
        // Validates if trip can be matched
        val = await this.validateMatch(trip, m);
        if ( val)
          match = m;
          break;
      }

      return match;
    }

    async findTripByMatchIdTripId(matchId: string, tripId: string): Promise<TripEntity> {
        const trip: TripEntity = await this.tripRepository.findOne({where: {id: tripId}});
        if (!trip)
          throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND)
        
        const match: MatchEntity = await this.matchRepository.findOne({where: {id: matchId}, relations: ["trips"]}); 
        if (!match)
          throw new BusinessLogicException("The match with the given id was not found", BusinessError.NOT_FOUND)
    
        const matchTrip: TripEntity = match.trips.find(e => e.id === trip.id);
    
        if (!matchTrip)
          throw new BusinessLogicException("The trip with the given id is not associated to the match", BusinessError.PRECONDITION_FAILED)
    
        return matchTrip;
    }
     
    async findTripsByMatchId(matchId: string): Promise<TripEntity[]> {
        const match: MatchEntity = await this.matchRepository.findOne({where: {id: matchId}, relations: ["trips"]});
        if (!match)
          throw new BusinessLogicException("The match with the given id was not found", BusinessError.NOT_FOUND)
        
        return match.trips;
    }
     
    async associateTripsMatch(matchId: string, trips: TripEntity[]): Promise<MatchEntity> {
        const match: MatchEntity = await this.matchRepository.findOne({where: {id: matchId}, relations: ["trips"]});
     
        if (!match)
          throw new BusinessLogicException("The match with the given id was not found", BusinessError.NOT_FOUND)
     
        for (let i = 0; i < trips.length; i++) {
          const trip: TripEntity = await this.tripRepository.findOne({where: {id: trips[i].id}});
          if (!trip)
            throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND)
        }
     
        match.trips = trips;
        return await this.matchRepository.save(match);
    }
     
    async deleteTripMatch(matchId: string, tripId: string){
        const trip: TripEntity = await this.tripRepository.findOne({where: {id: tripId}});
        if (!trip)
          throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND)
     
        const match: MatchEntity = await this.matchRepository.findOne({where: {id: matchId}, relations: ["trips"]});
        if (!match)
          throw new BusinessLogicException("The match with the given id was not found", BusinessError.NOT_FOUND)
     
        const matchTrip: TripEntity = match.trips.find(e => e.id === trip.id);
     
        if (!matchTrip)
            throw new BusinessLogicException("The trip with the given id is not associated to the match", BusinessError.PRECONDITION_FAILED)

        match.trips = match.trips.filter(e => e.id !== tripId);
        await this.matchRepository.save(match);
    }    
}
