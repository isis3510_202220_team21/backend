import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MatchEntity } from '../match/match.entity';
import { TripEntity } from '../trip/trip.entity';
import { MatchTripsService } from './match-trips.service';
import { MatchTripsController } from './match-trips.controller';

@Module({
  imports: [TypeOrmModule.forFeature([MatchEntity, TripEntity])],
  providers: [MatchTripsService],
  controllers: [MatchTripsController]
})
export class MatchTripsModule {}
