import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BusinessError, BusinessLogicException } from '../shared/errors/business-errors';
import { Repository } from 'typeorm';
import { UserEntity } from './user.entity';
import * as bcrypt from 'bcrypt';
import { LoginDto } from './login.dto';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>
    ){}

    async findAll(): Promise<UserEntity[]> {
        return await this.userRepository.find({ relations: ["reports", "trips"] });
    }

    async findOne(id: string): Promise<UserEntity> {
        const user: UserEntity = await this.userRepository.findOne({where: {id}, relations: ["reports", "trips"] } );
        if (!user)
          throw new BusinessLogicException("The user with the given id was not found", BusinessError.NOT_FOUND);
    
        return user;
    }

    async findOneByIdCard(idC: string): Promise<UserEntity> {
        const user: UserEntity = await this.userRepository.findOne({
            where: {
                idCard: idC
            }, 
            relations: ["reports", "trips"] 
        });
        if (!user)
          throw new BusinessLogicException("The user with the given idCard was not found", BusinessError.NOT_FOUND);
    
        return user;
    }

    async create(user: UserEntity): Promise<any> {
        const hashedPassword = await bcrypt.hash(user.password, 10);
        user.rating = 5;
        user.totalRating = 1;
        try {
            user.password = hashedPassword;
            const persistedUser = await this.userRepository.save(user);
            const { password, ...result } = persistedUser;
            return result;
        } catch (error) {
          if (error?.code === BusinessError.UniqueViolation) {
            throw new BusinessLogicException('User with that ID Card or email already exists', BusinessError.BAD_REQUEST);
            }
        }
    }

    private async verifyPassword(plainTextPassword: string, hashedPassword: string) {
        const isPasswordMatching = await bcrypt.compare(
            plainTextPassword,
            hashedPassword
        );
        if (!isPasswordMatching) {
            throw new BusinessLogicException('Wrong credentials provided', BusinessError.BAD_REQUEST);
        }
    }
    
    async login(loginDto: LoginDto): Promise<any> {
        try {
            const user = await this.findOneByIdCard(loginDto.idCard);
            await this.verifyPassword(loginDto.password, user.password);
            //const prueba = switchMap((userI: UserI) => this.authService.login(userI));
            
            if(user) {
                const { password, ...result } = user;
                return 'Login was succesfull';
            }
            
        } catch (error) {
            throw new BusinessLogicException('Wrong credentials provided', BusinessError.BAD_REQUEST);
        }
    }

    async update(id: string, user: UserEntity): Promise<UserEntity> {
        const persistedUser: UserEntity = await this.userRepository.findOne({where:{id}});
        if (!persistedUser)
          throw new BusinessLogicException("The user with the given id was not found", BusinessError.NOT_FOUND);
        
        return await this.userRepository.save({...persistedUser, ...user});
    }

    async updateScore(id:string, score:number):Promise<UserEntity> {
        const persistedUser: UserEntity = await this.userRepository.findOne({where:{id}});
        if (!persistedUser)
          throw new BusinessLogicException("The user with the given id was not found", BusinessError.NOT_FOUND);
        persistedUser.rating =  ((persistedUser.rating*persistedUser.totalRating)+score)/persistedUser.totalRating+1;
        persistedUser.totalRating = persistedUser.totalRating+1;
        return await this.update(id, persistedUser);
    }

    async delete(id: string) {
        const user: UserEntity = await this.userRepository.findOne({where:{id}});
        if (!user)
          throw new BusinessLogicException("The user with the given id was not found", BusinessError.NOT_FOUND);
      
        await this.userRepository.remove(user);
    }
}