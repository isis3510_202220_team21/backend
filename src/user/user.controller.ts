import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, UseInterceptors, Req, UseGuards  } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { BusinessErrorsInterceptor } from '../shared/interceptors/business-errors.interceptor';
import { UserDto } from './user.dto';
import { UserEntity } from './user.entity';
import { UserService } from './user.service';
import { LoginDto } from './login.dto';

@Controller('users')
@UseInterceptors(BusinessErrorsInterceptor)
export class UserController {
    constructor( private readonly userService: UserService ) {}

    @Get()
    async findAll() {
        return await this.userService.findAll();
    }

    @Get(':userId')
    async findOne(@Param('userId') userId: string) {
        return await this.userService.findOne(userId);
    }

    @Get('/cedula/:idCard')
    async findByIdCard(@Param('idCard') idCard : string){
        return await this.userService.findOneByIdCard(idCard);
    }

    @Post('register')
    async create(@Body() userDto: UserDto) {
        const user: UserEntity = plainToInstance(UserEntity, userDto);
        return await this.userService.create(user);
    }

    @Post('login')
    async login(@Body() loginDto: LoginDto) {
        return this.userService.login(loginDto);
    }

    @Put(':userId')
    async update(@Param('userId') userId: string, @Body() userDto: UserDto) {
        const user: UserEntity = plainToInstance(UserEntity, userDto);
        return await this.userService.update(userId, user);
    }

    @Delete(':userId')
    @HttpCode(204)
    async delete(@Param('userId') userId: string) {
        return await this.userService.delete(userId);
    }
}