import {IsNotEmpty, IsString} from 'class-validator';

export class LoginDto {
    @IsString()
    @IsNotEmpty()
    readonly idCard: string;

    @IsString()
    @IsNotEmpty()
    readonly password: string;
}
