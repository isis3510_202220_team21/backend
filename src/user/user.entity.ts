import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { ReportEntity } from '../report/report.entity';
import { TripEntity } from '../trip/trip.entity';

@Entity()
export class UserEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    name: string;

    @Column({unique: true})
    idCard: string;

    @Column()
    password: string;

    @Column({unique: true})
    email: string;

    @Column()
    rating: number;

    // Records the total Rating of the user
    @Column()
    totalRating : number;

    @Column()
    emergencyContacts: string;

    @OneToMany(() => ReportEntity, report => report.user)
    reports: ReportEntity[];

    @OneToMany(() => TripEntity, trip => trip.user)
    trips: TripEntity[];
}
