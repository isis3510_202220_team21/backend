import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { UserService } from '../user/user.service';
import { UserEntity } from '../user/user.entity';
import { BusinessError, BusinessLogicException } from '../shared/errors/business-errors';
import { JwtService } from '@nestjs/jwt';
import constants from '../shared/security/constants';
import { UserI } from '../user/user.interface';

@Injectable()
export class AuthService {
    constructor(
        //private readonly userService: UserService,
        private readonly jwtService: JwtService,) {}

    /* async register(user: UserEntity) {
        const hashedPassword = await bcrypt.hash(user.password, 10);
        try {
            user.password = hashedPassword;
            await this.userService.create(user);
            const { password, ...result } = user;
            return result;
        } catch (error) {
          if (error?.code === BusinessError.UniqueViolation) {
            throw new BusinessLogicException('User with that ID Card already exists', BusinessError.BAD_REQUEST);
            }
        }
    } */

    /* private async verifyPassword(plainTextPassword: string, hashedPassword: string) {
        const isPasswordMatching = await bcrypt.compare(
            plainTextPassword,
            hashedPassword
        );
        if (!isPasswordMatching) {
            throw new BusinessLogicException('Wrong credentials provided', BusinessError.BAD_REQUEST);
        }
    } */
    
    /* async validateUser(idCard: string, plainTextPassword: string): Promise<any> {
        try {
            const user = await this.userService.findOneByIdCard(idCard);
            await this.verifyPassword(plainTextPassword, user.password)
            const { password, ...result } = user;
            return result;
        } catch (error) {
            throw new BusinessLogicException('Wrong credentials provided', BusinessError.BAD_REQUEST);
        }
    } */

    async login(user: UserI) {
        //const payload = { name: req.user.name, sub: req.user.id };
        return {
            token: this.jwtService.sign(user, { privateKey: constants.JWT_SECRET }),
        };
        //return from(this.jwtService.signAsync(user, {privateKey: constants.JWT_SECRET}));
    }
}
