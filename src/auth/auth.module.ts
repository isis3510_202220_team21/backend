import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { UserModule } from '../user/user.module';
import { AuthService } from './auth.service';
import { JwtModule, JwtService } from '@nestjs/jwt';
import jwtConstants from '../shared/security/constants';
import { UserService } from '../user/user.service';
import { JwtStrategy } from './strategies/JwtStrategy';
import { UserEntity } from 'src/user/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    UserModule, 
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.JWT_SECRET,
      signOptions: { expiresIn: jwtConstants.JWT_EXPIRES_IN },
    }),
    TypeOrmModule.forFeature([UserEntity])],
  providers: [AuthService, UserService, JwtService, JwtStrategy],
  exports: [AuthService]
})
export class AuthModule {}
