import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { UserEntity } from '../user/user.entity';
import { StationEntity } from '../station/station.entity';

@Entity()
export class ReportEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    type: string;

    @Column()
    details: string;

    @Column()
    dateHour: string;

    @ManyToOne(() => UserEntity, user => user.reports)
    user: UserEntity;

    @ManyToOne(() => StationEntity, station => station.reports)
    station: StationEntity;
}
