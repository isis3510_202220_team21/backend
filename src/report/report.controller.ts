import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, UseInterceptors } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { BusinessErrorsInterceptor } from '../shared/interceptors/business-errors.interceptor';
import { ReportDto } from './report.dto';
import { ReportEntity } from './report.entity';
import { ReportService } from './report.service';

@Controller('reports')
@UseInterceptors(BusinessErrorsInterceptor)
export class ReportController {
    constructor(private readonly reportService: ReportService) {}

    @Get()
    async findAll() {
        return await this.reportService.findAll();
    }

    @Get(':reportId')
    async findOne(@Param('reportId') reportId: string) {
        return await this.reportService.findOne(reportId);
    }

    @Post()
    async create(@Body() reportDto: ReportDto) {
        const report: ReportEntity = plainToInstance(ReportEntity, reportDto);
        return await this.reportService.create(report);
    }

    @Put(':reportId')
    async update(@Param('reportId') reportId: string, @Body() reportDto: ReportDto) {
        const report: ReportEntity = plainToInstance(ReportEntity, reportDto);
        return await this.reportService.update(reportId, report);
    }

    @Delete(':reportId')
    @HttpCode(204)
    async delete(@Param('reportId') reportId: string) {
        return await this.reportService.delete(reportId);
    }
}
