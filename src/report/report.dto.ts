import {IsNotEmpty, IsString} from 'class-validator';

export class ReportDto {
    @IsString()
    @IsNotEmpty()
    readonly type: string;

    @IsString()
    @IsNotEmpty()
    readonly details: string;

    @IsString()
    @IsNotEmpty()
    readonly dateHour: string;
}
