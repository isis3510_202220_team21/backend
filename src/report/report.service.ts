import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BusinessError, BusinessLogicException } from '../shared/errors/business-errors';
import { Repository } from 'typeorm';
import { ReportEntity } from './report.entity';

@Injectable()
export class ReportService {
    constructor(
        @InjectRepository(ReportEntity)
        private readonly reportRepository: Repository<ReportEntity>
    ){}

    async findAll(): Promise<ReportEntity[]> {
        return await this.reportRepository.find({ relations: ["user", "station"] });
    }

    // Returns the lenght of all the reports in the database
    async totalReports(): Promise<number> {
        var reports = await this.findAll();
        return reports.length;
    }

    async findOne(id: string): Promise<ReportEntity> {
        const report: ReportEntity = await this.reportRepository.findOne({where: {id}, relations: ["user", "station"] } );
        if (!report)
          throw new BusinessLogicException("The report with the given id was not found", BusinessError.NOT_FOUND);
    
        return report;
    }

    async create(report: ReportEntity): Promise<ReportEntity> {
        return await this.reportRepository.save(report);
    }

    async update(id: string, report: ReportEntity): Promise<ReportEntity> {
        const persistedReport: ReportEntity = await this.reportRepository.findOne({where:{id}});
        if (!persistedReport)
          throw new BusinessLogicException("The report with the given id was not found", BusinessError.NOT_FOUND);
        
        return await this.reportRepository.save({...persistedReport, ...report});
    }

    async delete(id: string) {
        const report: ReportEntity = await this.reportRepository.findOne({where:{id}});
        if (!report)
          throw new BusinessLogicException("The report with the given id was not found", BusinessError.NOT_FOUND);
      
        await this.reportRepository.remove(report);
    }
}
