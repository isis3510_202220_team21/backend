import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../user/user.entity';
import { Repository } from 'typeorm';
import { BusinessError, BusinessLogicException } from '../shared/errors/business-errors';
import { ReportEntity } from '../report/report.entity';

@Injectable()
export class UserReportsService {
    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,

        @InjectRepository(ReportEntity)
        private readonly reportRepository: Repository<ReportEntity>
    ) {}

    async addReportUser(userId: string, reportId: string): Promise<UserEntity> {
        const report: ReportEntity = await this.reportRepository.findOne({where: {id: reportId}});
        if (!report)
          throw new BusinessLogicException("The report with the given id was not found", BusinessError.NOT_FOUND);
       
        const user: UserEntity = await this.userRepository.findOne({where: {id: userId}, relations: ["reports", "trips"]}) 
        if (!user)
          throw new BusinessLogicException("The user with the given id was not found", BusinessError.NOT_FOUND);
     
        user.reports = [...user.reports, report];
        return await this.userRepository.save(user);
    }
     
    async findReportByUserIdReportId(userId: string, reportId: string): Promise<ReportEntity> {
        const report: ReportEntity = await this.reportRepository.findOne({where: {id: reportId}});
        if (!report)
          throw new BusinessLogicException("The report with the given id was not found", BusinessError.NOT_FOUND)
        
        const user: UserEntity = await this.userRepository.findOne({where: {id: userId}, relations: ["reports"]}); 
        if (!user)
          throw new BusinessLogicException("The user with the given id was not found", BusinessError.NOT_FOUND)
    
        const userReport: ReportEntity = user.reports.find(e => e.id === report.id);
    
        if (!userReport)
          throw new BusinessLogicException("The report with the given id is not associated to the user", BusinessError.PRECONDITION_FAILED)
    
        return userReport;
    }
     
    async findReportsByUserId(userId: string): Promise<ReportEntity[]> {
        const user: UserEntity = await this.userRepository.findOne({where: {id: userId}, relations: ["reports"]});
        if (!user)
          throw new BusinessLogicException("The user with the given id was not found", BusinessError.NOT_FOUND)
        
        return user.reports;
    }
     
    async associateReportsUser(userId: string, reports: ReportEntity[]): Promise<UserEntity> {
        const user: UserEntity = await this.userRepository.findOne({where: {id: userId}, relations: ["reports"]});
     
        if (!user)
          throw new BusinessLogicException("The user with the given id was not found", BusinessError.NOT_FOUND)
     
        for (let i = 0; i < reports.length; i++) {
          const report: ReportEntity = await this.reportRepository.findOne({where: {id: reports[i].id}});
          if (!report)
            throw new BusinessLogicException("The report with the given id was not found", BusinessError.NOT_FOUND)
        }
     
        user.reports = reports;
        return await this.userRepository.save(user);
    }
     
    async deleteReportUser(userId: string, reportId: string){
        const report: ReportEntity = await this.reportRepository.findOne({where: {id: reportId}});
        if (!report)
          throw new BusinessLogicException("The report with the given id was not found", BusinessError.NOT_FOUND)
     
        const user: UserEntity = await this.userRepository.findOne({where: {id: userId}, relations: ["reports"]});
        if (!user)
          throw new BusinessLogicException("The user with the given id was not found", BusinessError.NOT_FOUND)
     
        const userReport: ReportEntity = user.reports.find(e => e.id === report.id);
     
        if (!userReport)
            throw new BusinessLogicException("The report with the given id is not associated to the user", BusinessError.PRECONDITION_FAILED)

        user.reports = user.reports.filter(e => e.id !== reportId);
        await this.userRepository.save(user);
    }   
}
