import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, UseInterceptors } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { ReportDto } from '../report/report.dto';
import { ReportEntity } from '../report/report.entity';
import { BusinessErrorsInterceptor } from '../shared/interceptors/business-errors.interceptor';
import { UserReportsService } from './user-reports.service';

@Controller('users')
@UseInterceptors(BusinessErrorsInterceptor)
export class UserReportsController {
    constructor(private readonly userReportService: UserReportsService){}

    @Post(':userId/reports/:reportId')
    async addReportUser(@Param('userId') userId: string, @Param('reportId') reportId: string){
        return await this.userReportService.addReportUser(userId, reportId);
    }

    @Get(':userId/reports/:reportId')
    async findReportByUserIdReportId(@Param('userId') userId: string, @Param('reportId') reportId: string){
        return await this.userReportService.findReportByUserIdReportId(userId, reportId);
    }

    @Get(':userId/reports')
    async findReportsByUserId(@Param('userId') userId: string){
        return await this.userReportService.findReportsByUserId(userId);
    }

    @Put(':userId/reports')
    async associateReportsUser(@Body() reportsDto: ReportDto[], @Param('userId') userId: string){
        const reports = plainToInstance(ReportEntity, reportsDto)
        return await this.userReportService.associateReportsUser(userId, reports);
    }
    
    @Delete(':userId/reports/:reportId')
    @HttpCode(204)
    async deleteReportUser(@Param('userId') userId: string, @Param('reportId') reportId: string){
        return await this.userReportService.deleteReportUser(userId, reportId);
    }
}