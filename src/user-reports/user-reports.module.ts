import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReportEntity } from '../report/report.entity';
import { UserEntity } from '../user/user.entity';
import { UserReportsService } from './user-reports.service';
import { UserReportsController } from './user-reports.controller';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity, ReportEntity])],
  providers: [UserReportsService],
  controllers: [UserReportsController]
})
export class UserReportsModule {}
