import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { BusinessError, BusinessLogicException } from '../shared/errors/business-errors';
import { InjectRepository } from '@nestjs/typeorm';
import { StationEntity } from '../station/station.entity';
import { BusEntity } from '../bus/bus.entity';

@Injectable()
export class StationBusesService {
    constructor(
        @InjectRepository(StationEntity)
        private readonly stationRepository: Repository<StationEntity>,

        @InjectRepository(BusEntity)
        private readonly busRepository: Repository<BusEntity>
    ) {}

    async addBusStation(stationId: string, busId: string): Promise<StationEntity> {
        const bus: BusEntity = await this.busRepository.findOne({where: {id: busId}});
        if (!bus)
          throw new BusinessLogicException("The bus with the given id was not found", BusinessError.NOT_FOUND);
       
        const station: StationEntity = await this.stationRepository.findOne({where: {id: stationId}, relations: ["reports", "buses"]}) 
        if (!station)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND);
     
        station.buses = [...station.buses, bus];
        return await this.stationRepository.save(station);
    }
     
    async findBusByStationIdBusId(stationId: string, busId: string): Promise<BusEntity> {
        const bus: BusEntity = await this.busRepository.findOne({where: {id: busId}});
        if (!bus)
          throw new BusinessLogicException("The bus with the given id was not found", BusinessError.NOT_FOUND)
        
        const station: StationEntity = await this.stationRepository.findOne({where: {id: stationId}, relations: ["buses"]}); 
        if (!station)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND)
    
        const stationBus: BusEntity = station.buses.find(e => e.id === bus.id);
    
        if (!stationBus)
          throw new BusinessLogicException("The bus with the given id is not associated to the station", BusinessError.PRECONDITION_FAILED)
    
        return stationBus;
    }
     
    async findBussByStationId(stationId: string): Promise<BusEntity[]> {
        const station: StationEntity = await this.stationRepository.findOne({where: {id: stationId}, relations: ["buses"]});
        if (!station)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND)
        
        return station.buses;
    }
     
    async associateBussStation(stationId: string, buss: BusEntity[]): Promise<StationEntity> {
        const station: StationEntity = await this.stationRepository.findOne({where: {id: stationId}, relations: ["buses"]});
     
        if (!station)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND)
     
        for (let i = 0; i < buss.length; i++) {
          const bus: BusEntity = await this.busRepository.findOne({where: {id: buss[i].id}});
          if (!bus)
            throw new BusinessLogicException("The bus with the given id was not found", BusinessError.NOT_FOUND)
        }
     
        station.buses = buss;
        return await this.stationRepository.save(station);
    }
     
    async deleteBusStation(stationId: string, busId: string){
        const bus: BusEntity = await this.busRepository.findOne({where: {id: busId}});
        if (!bus)
          throw new BusinessLogicException("The bus with the given id was not found", BusinessError.NOT_FOUND)
     
        const station: StationEntity = await this.stationRepository.findOne({where: {id: stationId}, relations: ["buses"]});
        if (!station)
          throw new BusinessLogicException("The station with the given id was not found", BusinessError.NOT_FOUND)
     
        const stationBus: BusEntity = station.buses.find(e => e.id === bus.id);
     
        if (!stationBus)
            throw new BusinessLogicException("The bus with the given id is not associated to the station", BusinessError.PRECONDITION_FAILED)

        station.buses = station.buses.filter(e => e.id !== busId);
        await this.stationRepository.save(station);
    }  
}
