import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BusEntity } from '../bus/bus.entity';
import { StationEntity } from '../station/station.entity';
import { StationBusesService } from './station-buses.service';
import { StationBusesController } from './station-buses.controller';

@Module({
  imports: [TypeOrmModule.forFeature([StationEntity, BusEntity])],
  providers: [StationBusesService],
  controllers: [StationBusesController]
})
export class StationBusesModule {}
