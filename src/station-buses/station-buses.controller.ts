import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, UseInterceptors } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { BusDto } from '../bus/bus.dto';
import { BusEntity } from '../bus/bus.entity';
import { BusinessErrorsInterceptor } from '../shared/interceptors/business-errors.interceptor';
import { StationBusesService } from './station-buses.service';

@Controller('stations')
@UseInterceptors(BusinessErrorsInterceptor)
export class StationBusesController {
    constructor(private readonly stationBusService: StationBusesService){}

    @Post(':stationId/buses/:busId')
    async addBusStation(@Param('stationId') stationId: string, @Param('busId') busId: string){
        return await this.stationBusService.addBusStation(stationId, busId);
    }

    @Get(':stationId/buses/:busId')
    async findBusByStationIdBusId(@Param('stationId') stationId: string, @Param('busId') busId: string){
        return await this.stationBusService.findBusByStationIdBusId(stationId, busId);
    }

    @Get(':stationId/buses')
    async findBussByStationId(@Param('stationId') stationId: string){
        return await this.stationBusService.findBussByStationId(stationId);
    }

    @Put(':stationId/buses')
    async associateBussStation(@Body() busesDto: BusDto[], @Param('stationId') stationId: string){
        const buses = plainToInstance(BusEntity, busesDto)
        return await this.stationBusService.associateBussStation(stationId, buses);
    }
    
    @Delete(':stationId/buses/:busId')
    @HttpCode(204)
    async deleteBusStation(@Param('stationId') stationId: string, @Param('busId') busId: string){
        return await this.stationBusService.deleteBusStation(stationId, busId);
    }
}
