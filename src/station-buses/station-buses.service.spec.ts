import { Test, TestingModule } from '@nestjs/testing';
import { StationBusesService } from './station-buses.service';

describe('StationBusesService', () => {
  let service: StationBusesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StationBusesService],
    }).compile();

    service = module.get<StationBusesService>(StationBusesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
