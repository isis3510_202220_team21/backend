import { Module } from '@nestjs/common';
import { UserTripsService } from './user-trips.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '../user/user.entity';
import { TripEntity } from '../trip/trip.entity';
import { UserTripsController } from './user-trips.controller';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity, TripEntity])],
  providers: [UserTripsService],
  controllers: [UserTripsController]
})
export class UserTripsModule {}
