import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { BusinessError, BusinessLogicException } from '../shared/errors/business-errors';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../user/user.entity';
import { TripEntity } from '../trip/trip.entity';

@Injectable()
export class UserTripsService {
    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,

        @InjectRepository(TripEntity)
        private readonly tripRepository: Repository<TripEntity>
    ) {}

    async addTripUser(userId: string, tripId: string): Promise<UserEntity> {
        const trip: TripEntity = await this.tripRepository.findOne({where: {id: tripId}});
        if (!trip)
          throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND);
       
        const user: UserEntity = await this.userRepository.findOne({where: {id: userId}, relations: ["reports", "trips"]}) 
        if (!user)
          throw new BusinessLogicException("The user with the given id was not found", BusinessError.NOT_FOUND);
     
        user.trips = [...user.trips, trip];
        return await this.userRepository.save(user);
    }
     
    async findTripByUserIdTripId(userId: string, tripId: string): Promise<TripEntity> {
        const trip: TripEntity = await this.tripRepository.findOne({where: {id: tripId}});
        if (!trip)
          throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND)
        
        const user: UserEntity = await this.userRepository.findOne({where: {id: userId}, relations: ["trips"]}); 
        if (!user)
          throw new BusinessLogicException("The user with the given id was not found", BusinessError.NOT_FOUND)
    
        const userTrip: TripEntity = user.trips.find(e => e.id === trip.id);
    
        if (!userTrip)
          throw new BusinessLogicException("The trip with the given id is not associated to the user", BusinessError.PRECONDITION_FAILED)
    
        return userTrip;
    }
     
    async findTripsByUserId(userId: string): Promise<TripEntity[]> {
        const user: UserEntity = await this.userRepository.findOne({where: {id: userId}, relations: ["trips"]});
        if (!user)
          throw new BusinessLogicException("The user with the given id was not found", BusinessError.NOT_FOUND)
        
        return user.trips;
    }
     
    async associateTripsUser(userId: string, trips: TripEntity[]): Promise<UserEntity> {
        const user: UserEntity = await this.userRepository.findOne({where: {id: userId}, relations: ["trips"]});
     
        if (!user)
          throw new BusinessLogicException("The user with the given id was not found", BusinessError.NOT_FOUND)
     
        for (let i = 0; i < trips.length; i++) {
          const trip: TripEntity = await this.tripRepository.findOne({where: {id: trips[i].id}});
          if (!trip)
            throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND)
        }
     
        user.trips = trips;
        return await this.userRepository.save(user);
    }
     
    async deleteTripUser(userId: string, tripId: string){
        const trip: TripEntity = await this.tripRepository.findOne({where: {id: tripId}});
        if (!trip)
          throw new BusinessLogicException("The trip with the given id was not found", BusinessError.NOT_FOUND)
     
        const user: UserEntity = await this.userRepository.findOne({where: {id: userId}, relations: ["trips"]});
        if (!user)
          throw new BusinessLogicException("The user with the given id was not found", BusinessError.NOT_FOUND)
     
        const userTrip: TripEntity = user.trips.find(e => e.id === trip.id);
     
        if (!userTrip)
            throw new BusinessLogicException("The trip with the given id is not associated to the user", BusinessError.PRECONDITION_FAILED)

        user.trips = user.trips.filter(e => e.id !== tripId);
        await this.userRepository.save(user);
    }  
}
