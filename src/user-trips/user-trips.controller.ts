import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, UseInterceptors } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { TripDto } from '../trip/trip.dto';
import { TripEntity } from '../trip/trip.entity';
import { BusinessErrorsInterceptor } from '../shared/interceptors/business-errors.interceptor';
import { UserTripsService } from './user-trips.service';

@Controller('users')
@UseInterceptors(BusinessErrorsInterceptor)
export class UserTripsController {
    constructor(private readonly userTripService: UserTripsService){}

    @Post(':userId/trips/:tripId')
    async addTripUser(@Param('userId') userId: string, @Param('tripId') tripId: string){
        return await this.userTripService.addTripUser(userId, tripId);
    }

    @Get(':userId/trips/:tripId')
    async findTripByUserIdTripId(@Param('userId') userId: string, @Param('tripId') tripId: string){
        return await this.userTripService.findTripByUserIdTripId(userId, tripId);
    }

    @Get(':userId/trips')
    async findTripsByUserId(@Param('userId') userId: string){
        return await this.userTripService.findTripsByUserId(userId);
    }

    @Put(':userId/trips')
    async associateTripsUser(@Body() tripsDto: TripDto[], @Param('userId') userId: string){
        const trips = plainToInstance(TripEntity, tripsDto)
        return await this.userTripService.associateTripsUser(userId, trips);
    }
    
    @Delete(':userId/trips/:tripId')
    @HttpCode(204)
    async deleteTripUser(@Param('userId') userId: string, @Param('tripId') tripId: string){
        return await this.userTripService.deleteTripUser(userId, tripId);
    }
}